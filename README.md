Slides and subtitle files for OpenSecurityTraining2 class material included in Architecture 100X level classes including "Architecture 1001: x86-64 Assembly"

Corrections to the subtitles are always welcome via merge requests
